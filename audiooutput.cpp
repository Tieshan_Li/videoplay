#include"audiooutput.h"
AudioOutput::AudioOutput(AVSync* avsync, AVRational time_base, const AudioParams& audio_params, AVFrameQueue* frame_queue){
    this->avsync = avsync;
    this->time_base = time_base;
    this->src_tgt = audio_params;
    this->frame_queue = frame_queue;
}

AudioOutput::~AudioOutput(){

}

FILE* dump_pcm = nullptr;
void fill_audio_pcm(void* udata, uint8_t* stream, int len){
    AudioOutput* is = (AudioOutput*)udata;
    int len1 = 0;
    int audio_size = 0;
    if(!dump_pcm){
        dump_pcm = fopen("dump.pcm", "wb");
    }
    while (len > 0)
    {
        if(is->audio_buf_index == is->audio_buf_size){
            is->audio_buf_index = 0;
            AVFrame* frame = is->frame_queue->Pop(10);
            if(frame){
                is->pts = frame->pts;
                if(frame->format != is->dst_tgt.fmt
                   || frame->sample_rate != is->dst_tgt.freq
                   || frame->channel_layout != is->dst_tgt.channel_layout
                   && !is->swr_ctx){

                    is->swr_ctx = swr_alloc_set_opts(NULL, 
                                                    is->dst_tgt.channel_layout,
                                                    (enum AVSampleFormat)is->dst_tgt.fmt,
                                                    is->dst_tgt.freq,
                                                    frame->channel_layout,
                                                    (enum AVSampleFormat)frame->format,
                                                    frame->sample_rate,
                                                    0, NULL);
                    if(!is->swr_ctx || swr_init(is->swr_ctx) < 0){
                        swr_free((SwrContext**)(&is->swr_ctx));
                        return;
                    }
                }
                if(is->swr_ctx){
                    const uint8_t** in = (const uint8_t**) frame->extended_data;
                    uint8_t** out = &is->audio_buf1;
                    int out_samples = frame->nb_samples * is->dst_tgt.freq / frame->sample_rate + 256;
                    int out_bytes = av_samples_get_buffer_size(NULL, is->dst_tgt.channels, out_samples, is->dst_tgt.fmt, 0);
                    if(out_bytes < 0){
                        return;
                    }
                    av_fast_malloc(&is->audio_buf1, &is->audio_buf1_size, out_bytes);
                    int len2 = swr_convert(is->swr_ctx, out, out_samples, in, frame->nb_samples);
                    if(len2 < 0){
                        return;
                    }
                    is->audio_buf = is->audio_buf1;
                    is->audio_buf_size = av_samples_get_buffer_size(NULL, is->dst_tgt.channels, len2, is->dst_tgt.fmt, 1);
                }else {
                    audio_size = av_samples_get_buffer_size(NULL, is->dst_tgt.channels, frame->nb_samples, (enum AVSampleFormat) frame->format, 1);
                    av_fast_malloc(&is->audio_buf1, &is->audio_buf1_size, audio_size);
                    is->audio_buf = is->audio_buf1;
                    is->audio_buf_size = audio_size;
                    memcpy(is->audio_buf, frame->data[0], audio_size);
                }
                av_frame_free(&frame);
            }else {
                is->audio_buf = nullptr;
                is->audio_buf_size = 512;
            }
        }
        len1 = is->audio_buf_size - is->audio_buf_index;
        if(len1 > len){
            len1 = len;
        }
        if(!is->audio_buf){
            memset(stream, 0, len1);
        }else {
            memcpy(stream, is->audio_buf + is->audio_buf_index, len1);
            fwrite((uint8_t*)is->audio_buf + is->audio_buf_index, 1, len1, dump_pcm);
            fflush(dump_pcm);
        }
        len -= len1;
        stream += len1;
        is->audio_buf_index += len1;
    }
    if(is->pts != AV_NOPTS_VALUE){
        double pts = is->pts * av_q2d(is->time_base);
        is->avsync->SetClock(pts);
    }
}

int AudioOutput::Init(){
    if(SDL_Init(SDL_INIT_AUDIO) != 0){
        return -1;
    }
    SDL_AudioSpec wanted_spec;
    SDL_AudioSpec spec;
    wanted_spec.channels = 2;
    wanted_spec.format = AUDIO_S16SYS;
    wanted_spec.silence = 0;
    wanted_spec.callback = fill_audio_pcm;
    wanted_spec.userdata = this;
    wanted_spec.samples = 1024;

    int ret = SDL_OpenAudio(&wanted_spec, nullptr);
    if(ret != 0){
        return -1;
    }
    dst_tgt.channels = wanted_spec.channels;
    dst_tgt.fmt = AV_SAMPLE_FMT_S16;
    dst_tgt.freq = wanted_spec.freq;
    dst_tgt.channel_layout = av_get_default_channel_layout(2);
    dst_tgt.frame_size = 1024;
    SDL_PauseAudio(0);
}

int AudioOutput::DeInit(){
    SDL_PauseAudio(1);
    SDL_CloseAudio();
}