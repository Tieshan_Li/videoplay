#ifndef AUDIOOUTPUT_H_
#define AUDIOOUTPUT_H_

#ifdef __cplusplus

extern "C"{
#include"libavutil/avutil.h"
#include"SDL.h"
#include"libswresample/swresample.h"
}

#include"avsync.h"
#include"avframequeue.h"

typedef struct AudioParams{
    int freq;
    int channels;
    int64_t channel_layout;
    enum AVSampleFormat fmt;
    int frame_size;
} AudioParams;

class AudioOutput
{
public:
    AudioOutput(AVSync* avsync, AVRational time_base, const AudioParams& audio_params, AVFrameQueue* frame_queue);
    ~AudioOutput();
    int Init();
    int DeInit();
public:
    int64_t pts = AV_NOPTS_VALUE;
    AudioParams src_tgt;
    AudioParams dst_tgt;
    AVFrameQueue* frame_queue = nullptr;
    struct SwrContext* swr_ctx = nullptr;
    uint8_t* audio_buf = nullptr;
    uint8_t* audio_buf1 = nullptr;
    uint32_t* audio_buf_size = 0;
    uint32_t* audio_buf1_size = 0;
    uint32_t* audio_buf_index = 0;
    AVSync* avsync = nullptr;
    AVRational time_base;
};

#endif

#endif