#include"avframequeue.h"
AVFrameQueue::AVFrameQueue(){

}

AVFrameQueue::~AVFrameQueue(){

}

void AVFrameQueue::Abort(){
    release();
    queue_t.Abort();
}

int AVFrameQueue::Push(AVFrame* val){
    return queue_t.Push(val);
}

AVFrame* AVFrameQueue::Pop(const int timeout){
    AVFrame* av_frame = nullptr;
    int ret = queue_t.Pop(av_frame, timeout);
    if(ret < 0){
        perror("AVFrameQueue:: Pop failed");
    }
    return av_frame;
}

AVFrame* AVFrameQueue::Front(){
    AVFrame* av_frame = nullptr;
    int ret = queue_t.Front(av_frame);
    if(ret < 0){
        perror("AVFrameQueue:: Front failed");
    }
    return av_frame;
}

int AVFrameQueue::Size(){
    return queue_t.Size();
}

void AVFrameQueue::release(){
    while(true){
        AVFrame* av_frame = nullptr;
        int ret = queue_t.Pop(av_frame, 1);
        if(ret < 0){
            break;
        }else {
            av_frame_free(&av_frame);
        }
    }
}