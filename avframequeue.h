#ifndef AVFRAMEQUEUE_H_
#define AVFRAMEQUEUE_H_

#include"queue.h"

#ifdef __cplusplus

extern "C" {
#include"libavutil/avutil.h"
#include"libavformat/avformat.h"
#include"libavcodec/avcodec.h"
}

#endif

class AVFrameQueue{
public:
    AVFrameQueue();
    ~AVFrameQueue();
    void Abort();
    int Push(AVFrame* val);
    AVFrame* Pop(const int timeout);
    AVFrame* Front();
    int Size();
private:
    void release();
    Queue<AVFrame*> queue_t;
};

#endif