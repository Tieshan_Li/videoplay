#include "AVPacketQueue.h"

AVPacketQueue::AVPacketQueue() {

}

AVPacketQueue::~AVPacketQueue() {

}

void AVPacketQueue::Abort() {
	release();
	my_queue.Abort();
}

int AVPacketQueue::Push(AVPacket* val) {
	return my_queue.Push(val);
}

int AVPacketQueue::Size() {
	return my_queue.Size();
}

AVPacket* AVPacketQueue::Pop(const int timeout) {
	AVPacket* avPack = nullptr;
	int ret = my_queue.Pop(avPack, timeout);
	if (ret < 0) {
		perror("AVPackeQueuet: Pop failed");
	} 
	return avPack;
}

void AVPacketQueue::release() {
	while (true)
	{
		AVPacket* packet = nullptr;
		int ret = my_queue.Pop(packet, 1);
		if(ret < 0){
			break;
		}else {
			av_packet_free(&packet);
		}
	}
}
