#pragma once
#ifndef AVPACKETQUEUE_H_
#define AVPACKETQUEUE_H_
#include "queue.h"

#ifdef __cplusplus

extern "C" {
#include"libavutil/avutil.h"
#include"libavformat/avformat.h"
#include"libavcodec/avcodec.h"
}

#endif // __cplusplus


class AVPacketQueue{
public:
	AVPacketQueue();
	~AVPacketQueue();
	void Abort();
	int Size();
	int Push(AVPacket* val);
	AVPacket* Pop(const int timeout);
private:
	void release();
	Queue<AVPacket*> my_queue;
};

#endif