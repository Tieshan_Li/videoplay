#ifndef AVSYNC_H_
#define AVSYNC_H_

#include<chrono>
#include<ctime>
#include<time.h>
#include<math.h>
using namespace std::chrono;


class AVSync
{
public:
    AVSync();
    void InitClock(){
        
    }
    void SetClockAt(double pts, double time){
        this->pts = pts;
        pts_drift = this->pts - time;
    }
    double GetClock(){
        double time = GetMicroseconds() / 1000000.0;
        return pts_drift + time;
    }
    double SetClock(){
        double time = GetMicroseconds() / 1000000.0;
        SetClockAt(pts, time);
    }
    time_t GetMicroseconds(){
        system_clock::time_point time_point_new = system_clock::now();
        system_clock::duration duration = time_point_new.time_since_epoch();
        time_t us = duration_cast<microseconds>(duration).count();
        return us;
    }
    double pts = 0;
    double pts_drift = 0;
};


#endif