#ifndef DECODETHREAD_H_
#define DECODETHREAD_H_

#include"thread.h"
#include"avpacketqueue.h"
#include"avframequeue.h"

class DecodeThread : public Thread
{
public:
    DecodeThread(AVPacketQueue* packet_queue, AVPacketQueue* frame_queue);
    ~DecodeThread();
    int Init(AVCodecParameters* par);
    int Start();
    int Stop();
    void Run();
private:
    AVCodecContext* codec_ctx = nullptr;
    AVPacketQueue* packet_queue = nullptr;
    AVFrameQueue* frame_queue = nullptr;
};

#endif