#pragma once
#ifndef DEMUXTHREAD_H_
#define DEMUXTHREAD_H_
#include"thread.h"
#include"avpacketqueue.h"

#ifdef __cplusplus

extern "C" {
#include"libavutil/avutil.h"
#include"libavformat/avformat.h"
}

#endif // __cplusplus


class DemuxThread : public Thread
{
public:
	DemuxThread(AVPacketQueue* audio_packet_q, AVPacketQueue* video_packet_q);
	~DemuxThread();
	int Init(const string& url);
	int Start();
	int Stop();
	void Run();
	AVCodecParameters* AudioCodecParameters();
	AVCodecParameters* VideoCodecParameters();
	AVRational AudioStreamTimebase();
	AVRational VideoStreamTimebase();
private:
	string url;
	AVFormatContext* ifmt_ctx = nullptr;
	AVPacketQueue* audio_packet_q = nullptr;
	AVPacketQueue* video_packet_q = nullptr;
	int audio_index = -1;
	int video_index = -1;
};

#endif