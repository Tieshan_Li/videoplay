#pragma once
#ifndef THREAD_H_
#define THREAD_H_
#include<thread>
using namespace std;

class Thread
{
public:
	Thread(){}
	~Thread() { 
		if (mythread) {
			Stop();
		}
	}
	int Start(){}
	int Stop() {
		abort = 1;
		if (mythread) {
			mythread->join();
			delete mythread;
			mythread = nullptr;
		}
	}
	virtual void Run() = 0;
protected:
	int abort = 0;
	thread* mythread = nullptr;
};

#endif