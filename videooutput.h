#ifndef VIDEOOUTPUT_H_
#define VIDEOOUTPUT_H_

#ifdef __cplusplus
extern "C"{
#include"libavutil/avutil.h"
#include"SDL.h"
#include"libavutil/time.h"
}
#endif

#include"avframequeue.h"
#include"avsync.h"

class VideoOutput{
public:
    VideoOutput(AVSync* avsync, AVRational time_base, AVFrameQueue* frame_queue, int video_width, int video_height);
    ~VideoOutput();
    int Init();
    int MainLoop();
    void RefreshLoopWaitEvent(SDL_Event* event);
private:
    void videoRefresh(double* remaining_time);
    AVFrameQueue* frame_queue = nullptr;
    SDL_Event event;
    SDL_Rect rect;
    SDL_Window* win = nullptr;
    SDL_Renderer* renderer = nullptr;
    SDL_Texture* texture = nullptr;

    AVSync* avsync = nullptr;
    AVRational time_base;

    int video_width = 0;
    int video_height = 0;
    uint8_t* yuv_buf = nullptr;
    int yuv_buf_size = 0;
};

#endif